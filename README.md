# Go three Redis

### Benchmark
```bash
go test ./... -v -bench=. -benchmem -count=10 > bench.txt
```
```bash
benchstat bench.txt
```
```text
name               time/op
Redis1Increment    8.36µs ± 2%
Redis2Increment    8.44µs ± 2%
Redis3Increment    8.71µs ± 3%
RedisAllIncrement  5.54µs ± 2%
Keydb1Increment    9.13µs ± 1%
Keydb2Increment    9.04µs ± 2%
Keydb3Increment    9.08µs ± 1%
KeydbAllIncrement  5.88µs ± 6%

name               alloc/op
Redis1Increment      393B ± 0%
Redis2Increment      393B ± 0%
Redis3Increment      393B ± 0%
RedisAllIncrement    394B ± 0%
Keydb1Increment      393B ± 0%
Keydb2Increment      393B ± 0%
Keydb3Increment      393B ± 0%
KeydbAllIncrement    394B ± 0%

name               allocs/op
Redis1Increment      7.00 ± 0%
Redis2Increment      7.00 ± 0%
Redis3Increment      7.00 ± 0%
RedisAllIncrement    7.00 ± 0%
Keydb1Increment      7.00 ± 0%
Keydb2Increment      7.00 ± 0%
Keydb3Increment      7.00 ± 0%
KeydbAllIncrement    7.00 ± 0%
```