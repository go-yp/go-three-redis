package main

import (
	"fmt"
	"github.com/go-redis/redis"
	"os"
)

// https://github.com/go-redis/redis#quickstart
func client(addr string) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     addr, // "localhost:6379",
		Password: "",   // no password set
		DB:       0,    // use default DB
	})

	err := client.Ping().Err()
	if err != nil {
		return nil, err
	}

	return client, err
}

func main() {
	if len(os.Args) != 3 {
		panic("sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb1:6379 1")
	}

	var (
		addr = os.Args[1]
		key  = os.Args[2]
	)

	var client, connectionErr = client(addr)
	if connectionErr != nil {
		panic(connectionErr)
	}

	var result, getErr = client.Get(key).Int()
	if getErr != nil {
		panic(getErr)
	}
	fmt.Println(result)
}
