package tests

import "github.com/go-redis/redis"

// https://github.com/go-redis/redis#quickstart
func Client(addr string) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     addr, // "localhost:6379",
		Password: "",   // no password set
		DB:       0,    // use default DB
	})

	err := client.Ping().Err()
	if err != nil {
		return nil, err
	}

	return client, err
}
