package tests

import (
	"sync/atomic"
	"testing"

	"github.com/go-redis/redis"
	"github.com/stretchr/testify/require"
)

var (
	keys = []string{
		"1",
		"2",
		"3",
		"4",
		"5",
	}
	keyCount = uint32(len(keys))
)

func BenchmarkRedis1Increment(b *testing.B) {
	benchmarkIncrement(b, "redis1:6379", "redis1:6379", "redis1:6379")
}

func BenchmarkRedis2Increment(b *testing.B) {
	benchmarkIncrement(b, "redis2:6379", "redis2:6379", "redis2:6379")
}

func BenchmarkRedis3Increment(b *testing.B) {
	benchmarkIncrement(b, "redis3:6379", "redis3:6379", "redis3:6379")
}

func BenchmarkRedisAllIncrement(b *testing.B) {
	benchmarkIncrement(b, "redis1:6379", "redis2:6379", "redis3:6379")
}

func BenchmarkKeydb1Increment(b *testing.B) {
	benchmarkIncrement(b, "keydb1:6379", "keydb1:6379", "keydb1:6379")
}

func BenchmarkKeydb2Increment(b *testing.B) {
	benchmarkIncrement(b, "keydb2:6379", "keydb2:6379", "keydb2:6379")
}

func BenchmarkKeydb3Increment(b *testing.B) {
	benchmarkIncrement(b, "keydb3:6379", "keydb3:6379", "keydb3:6379")
}

func BenchmarkKeydbAllIncrement(b *testing.B) {
	benchmarkIncrement(b, "keydb1:6379", "keydb2:6379", "keydb3:6379")
}

func BenchmarkDragonflydb1Increment(b *testing.B) {
	benchmarkIncrement(b, "dragonflydb1:6379", "dragonflydb1:6379", "dragonflydb1:6379")
}

func BenchmarkDragonflydb2Increment(b *testing.B) {
	benchmarkIncrement(b, "dragonflydb2:6379", "dragonflydb2:6379", "dragonflydb2:6379")
}

func BenchmarkDragonflydb3Increment(b *testing.B) {
	benchmarkIncrement(b, "dragonflydb3:6379", "dragonflydb3:6379", "dragonflydb3:6379")
}

func BenchmarkDragonflydbAllIncrement(b *testing.B) {
	benchmarkIncrement(b, "dragonflydb1:6379", "dragonflydb2:6379", "dragonflydb3:6379")
}

func benchmarkIncrement(b *testing.B, addrs ...string) {
	b.Helper()

	var (
		clientCount = uint32(len(addrs))
		clients     = make([]*redis.Client, clientCount)
	)

	for i, addr := range addrs {
		var client, err = Client(addr)
		require.NoError(b, err)
		defer client.Close()

		var flushAllErr = client.FlushAll().Err()
		require.NoError(b, flushAllErr)

		clients[i] = client
	}

	var counter = uint32(0)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var index = atomic.AddUint32(&counter, 1)

			var client = clients[index%clientCount]

			var err = client.Incr(keys[index%keyCount]).Err()

			require.NoError(b, err)
		}
	})
}
