up:
	sudo docker-compose up -d

down:
	sudo docker-compose down

app:
	sudo docker exec -it go_three_redis_app sh

bench:
	sudo docker exec go_three_redis_app go test ./... -v -bench=. -benchmem -count=10 | tee bench.txt
	benchstat bench.txt

redis1:
	sudo docker exec -it go_three_redis_1 redis-cli

redis2:
	sudo docker exec -it go_three_redis_2 redis-cli

redis3:
	sudo docker exec -it go_three_redis_3 redis-cli

keydb1:
	sudo docker exec -it go_three_keydb_1 keydb-cli

keydb2:
	sudo docker exec -it go_three_keydb_2 keydb-cli

keydb3:
	sudo docker exec -it go_three_keydb_3 keydb-cli

dragonflydb1:
	sudo docker exec -it go_three_dragonflydb_1 bash

dragonflydb2:
	sudo docker exec -it go_three_dragonflydb_2 bash

dragonflydb3:
	sudo docker exec -it go_three_dragonflydb_3 bash

state:
	# Redis
	sudo docker exec go_three_redis_1 redis-cli GET "1"
	sudo docker exec go_three_redis_1 redis-cli GET "2"
	sudo docker exec go_three_redis_1 redis-cli GET "3"
	sudo docker exec go_three_redis_1 redis-cli GET "4"
	sudo docker exec go_three_redis_1 redis-cli GET "5"
	sudo docker exec go_three_redis_2 redis-cli GET "1"
	sudo docker exec go_three_redis_2 redis-cli GET "2"
	sudo docker exec go_three_redis_2 redis-cli GET "3"
	sudo docker exec go_three_redis_2 redis-cli GET "4"
	sudo docker exec go_three_redis_2 redis-cli GET "5"
	sudo docker exec go_three_redis_3 redis-cli GET "1"
	sudo docker exec go_three_redis_3 redis-cli GET "2"
	sudo docker exec go_three_redis_3 redis-cli GET "3"
	sudo docker exec go_three_redis_3 redis-cli GET "4"
	sudo docker exec go_three_redis_3 redis-cli GET "5"
	# KeyDB
	sudo docker exec go_three_keydb_1 keydb-cli GET "1"
	sudo docker exec go_three_keydb_1 keydb-cli GET "2"
	sudo docker exec go_three_keydb_1 keydb-cli GET "3"
	sudo docker exec go_three_keydb_1 keydb-cli GET "4"
	sudo docker exec go_three_keydb_1 keydb-cli GET "5"
	sudo docker exec go_three_keydb_2 keydb-cli GET "1"
	sudo docker exec go_three_keydb_2 keydb-cli GET "2"
	sudo docker exec go_three_keydb_2 keydb-cli GET "3"
	sudo docker exec go_three_keydb_2 keydb-cli GET "4"
	sudo docker exec go_three_keydb_2 keydb-cli GET "5"
	sudo docker exec go_three_keydb_3 keydb-cli GET "1"
	sudo docker exec go_three_keydb_3 keydb-cli GET "2"
	sudo docker exec go_three_keydb_3 keydb-cli GET "3"
	sudo docker exec go_three_keydb_3 keydb-cli GET "4"
	sudo docker exec go_three_keydb_3 keydb-cli GET "5"
	# Dragonfly
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb1:6379 1
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb1:6379 2
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb1:6379 3
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb1:6379 4
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb1:6379 5
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb2:6379 1
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb2:6379 2
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb2:6379 3
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb2:6379 4
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb2:6379 5
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb3:6379 1
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb3:6379 2
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb3:6379 3
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb3:6379 4
	sudo docker exec go_three_redis_app go run ./cmd/get/main.go dragonflydb3:6379 5
